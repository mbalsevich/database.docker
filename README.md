# database.docker

Container para ejecutar MySQL y Postgresql.
# Antes de usar

1) Modificar la ruta de los directorios en los docker-compose.yml para que apunten a uno existente en la máquina local

2) Crear una red que permitirá conectarse desde otros containers con el host "{servicio}.database"
El comando para crear la red es:
docker network create database

3) Para conectar desde los otros dockers, los hosts para cada uno son:
"mysql.database" => MySQL (user:root, pass: ninguno)
"pg.database" => PostgreSQL (user:postgres, pass: 1234)

